import { LitElement, html } from '../web_modules/lit-element.js'

export class AppTitle extends LitElement {
  
  render() {
    return html`<h1>🖖 live long and prosper 🌍</h1>`
  }
}

customElements.define('app-title', AppTitle)