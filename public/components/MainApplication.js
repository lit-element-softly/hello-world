import { LitElement, html } from '../web_modules/lit-element.js'
import {} from './AppTitle.js'

export class MainApplication extends LitElement {

  render() {
    return html`
      <div>
        <app-title></app-title>
      </div>
    `
  }
}

customElements.define('main-application', MainApplication)